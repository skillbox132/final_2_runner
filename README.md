# final_2_runner

## О работе сценария
Terraform-сценарий репозитория производит развёртывание инфраструктуры в AWS (1 aws_security_group и 1 aws_instance) и развёртывания роли ansible, устанавливающей пакет gitlab-runner и производящий его сопряжение с целевым репозиторием.

## Требования к окружению локального компьютера:
- [aws-cli - 2.6.4 или более поздняя версия](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html#getting-started-install-instructions);
- [terraform - v1.2.3 или более поздняя версия](https://www.terraform.io/downloads);
- [ansible - core 2.12.6 или более поздняя версия](https://docs.ansible.com/ansible/latest/installation_guide/index.html).
- [сопряжение aws-cli с аккаунтом AWS](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html);
- [сгенерированная пара SSH-ключей доступа для Amazon EC2 instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html).

## Как развернуть инфраструктуру AWS:  
1. Склонировать репозиторий на локальный компьютер, следуя [этой инструкции](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-on-github/duplicating-a-repository#mirroring-a-repository-in-another-location).
2. Внести путь к своему приватному ключу доступа к AWS ec2 instance, скорректировав переменную private_key_file в файле terraform/ansible.cfg репозитория.
3. Ввести registration_token к репозиторию, на который устанавливается gitlab_runner, прописав его в поле default = "<YOUR_REGISTRATION_TOKEN>" файла terraform/variables.tf или указать его в шаге 4:
4. Из контекста директории terraform репозитория выполнить команду, в случае прописания registration_token в файле:
   ```shell
   terraform apply --auto-approve
   ```
   В ином случае команду:
   ```shell
   terraform apply --auto-approve -var="GITLAB_REGISTRATION_TOKEN=<YOUR_REGISTRATION_TOKEN>"
   ```
5. Убедиться в подключении gitlab_runner, в разделе settings - CI/CD - runners целевого репозитория.