provider "aws" {
  region = "us-east-1"
}

data "aws_ami" "debian-11" {
  most_recent = true
  owners      = ["136693071363"]
  filter {
    name   = "name"
    values = ["debian-11-amd64-*"]
  }
}

resource "aws_security_group" "runner" {
  name = "Runner Security Group"

  dynamic "ingress" {
    for_each = ["22"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "runner" {
  ami                    = data.aws_ami.debian-11.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.runner.id]
  key_name               = "AWS_2022_05_12"
  lifecycle {
    create_before_destroy = true
  }
  tags = {
    Name = "runner"
  }
  provisioner "local-exec" {
    command = "sleep 20 && ansible-playbook -i '${self.public_ip},' ../ansible/deploy_gitlab-runner.yaml --extra-var 'gitlab_registration_token=${var.GITLAB_REGISTRATION_TOKEN}'"
  }
}

